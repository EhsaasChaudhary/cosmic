import 'package:cosmic_app/Login.dart';
import 'package:flutter/material.dart';

class FirstScreen extends StatefulWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  State<FirstScreen> createState() => _FirstScreenState();
}

class _FirstScreenState extends State<FirstScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                image: const DecorationImage(
                    image: AssetImage("assets/images/allscreen-bg.png"),
                    fit: BoxFit.cover),
                borderRadius: BorderRadiusDirectional.circular(35),
              ),
            ),
          ),
          Center(
            child: Container(
              padding: const EdgeInsetsDirectional.only(bottom: 350),
              child: const Image(
                image: AssetImage("assets/images/loaderpic.png"),
                height: 296,
                width: 296,
              ),
            ),
          ),
          // Center(
          //   child: Container(
          //     padding: EdgeInsetsDirectional.only(top: 580),
          //     child: Image.asset("assets/images/fluttericon.png"),
          //   ),
          // ),
          Center(
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => const LoginScreen(),
                  ),
                );
              },
              child: Container(
                margin: const EdgeInsetsDirectional.only(top: 400),
                child: const Icon(
                  Icons.rocket_launch_sharp,
                  color: Colors.white,
                  size: 100,
                  shadows: [
                    Shadow(
                      offset: Offset(0, 2),
                      blurRadius: 40,
                      color: Colors.white,
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
