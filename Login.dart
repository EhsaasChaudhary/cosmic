import 'package:cosmic_app/Signup.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  image: const DecorationImage(
                      image: AssetImage("assets/images/allscreen-bg.png"),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadiusDirectional.circular(35),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsets.fromLTRB(80, 60, 80, 680),
              child: Image.asset("assets/images/logonew.png"),
            ),
            Container(
              margin: const EdgeInsetsDirectional.only(top: 270),
              decoration: BoxDecoration(
                color: const Color(0x90091522),
                // boxShadow: [
                //   BoxShadow(
                //     offset: Offset(0, -13),
                //     spreadRadius: -5,
                //     blurRadius: 1000,
                //     color: Color.fromRGBO(13, 31, 41, 1),
                //   )
                // ],
                borderRadius: BorderRadiusDirectional.circular(35),
                border: Border.all(
                  width: 1,
                  color: const Color(0xFF3A3A42),
                ),
              ),
            ),
            Container(
              padding: const EdgeInsetsDirectional.only(
                  top: 0, start: 45, end: 45, bottom: 60),
              margin: const EdgeInsetsDirectional.only(top: 300),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  const Text(
                    "Sign In",
                    style: TextStyle(
                        color: Colors.white70,
                        fontSize: 32,
                        fontWeight: FontWeight.w700),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color(0xDD3A3A42), width: 1),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      labelText: "Email",
                      labelStyle: const TextStyle(
                        color: Color(0xFF8D8E99),
                      ),
                    ),
                  ),
                  TextFormField(
                    decoration: InputDecoration(
                      enabledBorder: OutlineInputBorder(
                        borderSide: const BorderSide(
                            color: Color(0xDD3A3A42), width: 1),
                        borderRadius: BorderRadius.circular(30),
                      ),
                      labelText: "Password",
                      labelStyle: const TextStyle(
                        color: Color(0xFF8D8E99),
                      ),
                    ),
                  ),
                  TextButton(
                    onPressed: () {},
                    child: const Text(
                      "Forgot Password?",
                      style: TextStyle(
                        color: Color(0xFF11DCE8),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 279,
                    height: 44,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        gradient: const LinearGradient(
                          begin: Alignment.bottomLeft,
                          end: Alignment.topRight,
                          colors: [
                            Color(0x900AFBFB),
                            Color(0x7072A5F2),
                            Color(0x70E961FF)
                          ],
                        ),
                        borderRadius: BorderRadius.circular(44),
                      ),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          disabledBackgroundColor: Colors.transparent,
                          shadowColor: Colors.transparent,
                        ),
                        onPressed: () {
                          // Respond to button press
                        },
                        child: const Text(
                          "Sign In",
                          style: TextStyle(
                              color: Colors.white70,
                              fontSize: 24,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),
                  ),
                  const Text(
                    "----------------or sign in using----------------",
                    style: TextStyle(color: Color(0xFF8D8E99)),
                  ),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin:
                            const EdgeInsetsDirectional.only(start: 50, end: 5),
                        height: 48,
                        width: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: const Color(0xFF3A3A42),
                            ),
                            borderRadius: BorderRadius.circular(60),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.transparent,
                              disabledBackgroundColor: Colors.transparent,
                              shadowColor: Colors.transparent,
                            ),
                            onPressed: () {
                              // Respond to button press
                            },
                            child: const Icon(Icons.access_alarm),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsetsDirectional.only(start: 5, end: 5),
                        height: 48,
                        width: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: const Color(0xFF3A3A42),
                            ),
                            borderRadius: BorderRadius.circular(60),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.transparent,
                              disabledBackgroundColor: Colors.transparent,
                              shadowColor: Colors.transparent,
                            ),
                            onPressed: () {
                              // Respond to button press
                            },
                            child: const Icon(Icons.facebook_rounded),
                          ),
                        ),
                      ),
                      Container(
                        margin:
                            const EdgeInsetsDirectional.only(start: 5, end: 50),
                        height: 48,
                        width: 48,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(60),
                        ),
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            border: Border.all(
                              width: 1,
                              color: const Color(0xFF3A3A42),
                            ),
                            borderRadius: BorderRadius.circular(60),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.transparent,
                              disabledBackgroundColor: Colors.transparent,
                              shadowColor: Colors.transparent,
                            ),
                            onPressed: () {
                              // Respond to button press
                            },
                            child: const Icon(Icons.g_mobiledata_rounded),
                          ),
                        ),
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Text(
                        "Don't have an account?",
                        style:
                            TextStyle(color: Color(0xFF8D8E99), fontSize: 10),
                      ),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => const SignupScreen(),
                            ),
                          );
                        },
                        child: const Text(
                          "Sign Up",
                          style: TextStyle(
                            color: Color(0xFF11DCE8),
                            fontSize: 15,
                          ),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
