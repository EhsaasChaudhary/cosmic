import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';

class Homescreen extends StatefulWidget {
  const Homescreen({Key? key}) : super(key: key);

  Future<Map<String, dynamic>> getPlanetsData() async {
    http.Response res = await http.get(Uri.parse("https://api.le-systeme-solaire.net/rest/bodies/{Planet}"));
    Map<String, dynamic> map = jsonDecode(res.body.toString());
    return map;
  }

  @override
  State<Homescreen> createState() => _HomescreenState();
}

class _HomescreenState extends State<Homescreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Expanded(
          child: Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage("assets/images/allscreen-bg.png"),
                  fit: BoxFit.cover),
              // borderRadius: BorderRadiusDirectional.circular(35),
            ),
          ),
        ),
        Column(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  color: const Color(0x90091522),
                  borderRadius: const BorderRadiusDirectional.only(
                    bottomEnd: Radius.circular(35),
                    bottomStart: Radius.circular(35),
                  ),
                  border: Border.all(
                    width: 1,
                    color: const Color(0xFF3A3A42),
                  ),
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                      margin:
                          const EdgeInsetsDirectional.only(start: 24, top: 15),
                      height: 48,
                      width: 48,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          color: const Color(0x70091522),
                          borderRadius: BorderRadiusDirectional.circular(28),
                          border: Border.all(
                            width: 1,
                            color: const Color(0xFF3A3A42),
                          ),
                        ),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            disabledBackgroundColor: Colors.transparent,
                            shadowColor: Colors.transparent,
                          ),
                          onPressed: () {
                            // Respond to button press
                          },
                          child: const Icon(Icons.menu),
                        ),
                      ),
                    ),
                    Container(
                      margin:
                          const EdgeInsetsDirectional.only(end: 24, top: 15),
                      height: 48,
                      width: 48,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          color: const Color(0x70091522),
                          borderRadius: BorderRadiusDirectional.circular(28),
                          border: Border.all(
                            width: 1,
                            color: const Color(0xFF3A3A42),
                          ),
                        ),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            disabledBackgroundColor: Colors.transparent,
                            shadowColor: Colors.transparent,
                          ),
                          onPressed: () {
                            // Respond to button press
                          },
                          child: const Icon(Icons.person),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              flex: 2,
            ),
            Expanded(
              child: FutureBuilder<Map<String, dynamic>>(
                builder: (context, snapshot) {
                  if (snapshot.data != null && snapshot.hasData) {
                    return Row(children: [Expanded(child: Container(),),Expanded(child: Container(color: Colors.red,),)],);
                  } else {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              ),
              flex: 5,
            ),
            Expanded(
              child: Container(),
              flex: 5,
            ),
            Expanded(
              child: Container(
                decoration: const BoxDecoration(
                  color: Color(0xAA091522),
                  borderRadius: BorderRadiusDirectional.only(
                    topStart: Radius.circular(28),
                    topEnd: Radius.circular(28),
                  ),
                ),
                // child: BottomNavigationBar(
                //   backgroundColor: const Color(0xAA091522),
                //   items: const <BottomNavigationBarItem>[
                //     BottomNavigationBarItem(
                //       icon: Icon(Icons.home),
                //       label: 'Home',
                //     ),
                //     BottomNavigationBarItem(
                //       icon: Icon(Icons.favorite_border_outlined),
                //       label: 'Favourites',
                //     ),
                //     BottomNavigationBarItem(
                //       icon: Icon(Icons.more_horiz),
                //       label: "More",
                //     ),
                //   ],
                // ),
              ),
            )
          ],
        ),
      ],
    );
  }
}
