import 'package:cosmic_app/Home.dart';
import 'package:cosmic_app/Innerpage.dart';
import 'package:cosmic_app/Splash.dart';
import 'package:cosmic_app/profile.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        home: const Profilescreen());
  }
}
