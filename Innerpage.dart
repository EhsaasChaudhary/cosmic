import 'package:flutter/material.dart';

class InnerScreen extends StatefulWidget {
  const InnerScreen({Key? key}) : super(key: key);

  @override
  State<InnerScreen> createState() => _InnerScreenState();
}

class _InnerScreenState extends State<InnerScreen> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Stack(
          children: [
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  image: const DecorationImage(
                      image: AssetImage("assets/images/earth-bg.png"),
                      fit: BoxFit.cover),
                  borderRadius: BorderRadiusDirectional.circular(35),
                ),
              ),
            ),
            Expanded(
              child: Container(
                decoration: BoxDecoration(
                  gradient: const LinearGradient(
                    begin: Alignment.bottomLeft,
                    end: Alignment.topRight,
                    colors: [
                      Color(0x70E961FF),
                      Color(0x8072A5F2),
                      Color(0x100FFBFB)
                    ],
                  ),
                  borderRadius: BorderRadius.circular(35),
                ),
              ),
            ),
            Container(
              width: 375,
              margin: const EdgeInsetsDirectional.only(top: 20),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    margin: const EdgeInsetsDirectional.only(start: 24),
                    height: 48,
                    width: 48,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: const Color(0x70091522),
                        borderRadius: BorderRadiusDirectional.circular(60),
                        border: Border.all(
                          width: 1,
                          color: const Color(0xFF3A3A42),
                        ),
                      ),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          disabledBackgroundColor: Colors.transparent,
                          shadowColor: Colors.transparent,
                        ),
                        onPressed: () {
                          // Respond to button press
                        },
                        child: const Icon(Icons.arrow_back_outlined),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsetsDirectional.only(end: 24),
                    height: 48,
                    width: 48,
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: const Color(0x70091522),
                        borderRadius: BorderRadiusDirectional.circular(60),
                        border: Border.all(
                          width: 1,
                          color: const Color(0xFF3A3A42),
                        ),
                      ),
                      child: ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          backgroundColor: Colors.transparent,
                          disabledBackgroundColor: Colors.transparent,
                          shadowColor: Colors.transparent,
                        ),
                        onPressed: () {
                          // Respond to button press
                        },
                        child: const Icon(Icons.favorite_border_outlined),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsetsDirectional.only(top: 270),
              decoration: BoxDecoration(
                color: const Color(0x70091522),
                borderRadius: BorderRadiusDirectional.circular(35),
                border: Border.all(
                  width: 1,
                  color: const Color(
                    0xFF3A3A42,
                  ),
                ),
              ),
              child: Column(
                children: [
                  Container(
                    padding: EdgeInsetsDirectional.only(top: 70),
                    child: const Text(
                      "Earth",
                      style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.w700,
                          color: Colors.white),
                    ),
                  ),
                  Expanded(
                    child: Container(margin: EdgeInsetsDirectional.only(top: 28,start: 20,end: 20),
                      child: Row(
                        children: [
                          Expanded(
                            child: Column(
                              children: [
                                Expanded(
                                 child: Column(children: [Icon(Icons.balance,color: Colors.white,),Text("Mass",style: TextStyle(color: Colors.white),),Text("(10\u00B2\u2074kg)",style: TextStyle(color: Colors.white),),Text("5.97",style: TextStyle(color: Colors.white,fontSize: 30,fontWeight: FontWeight.bold),),],),
                                ),
                                Expanded(
                                  child: Column(children: [Icon(Icons.rocket_launch_outlined,color: Colors.white,),Text("Esc.Velocity",style: TextStyle(color: Colors.white),),Text("(km/s)",style: TextStyle(color: Colors.white),),Text("11.2",style: TextStyle(color: Colors.white,fontSize: 30,fontWeight: FontWeight.bold),),],)
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Expanded(
                                  child:Column(children: [Icon(Icons.arrow_circle_down_outlined,color: Colors.white,),Text("Gravity",style: TextStyle(color: Colors.white),),Text("(m/s\u00B2)",style: TextStyle(color: Colors.white),),Text("9.8",style: TextStyle(color: Colors.white,fontSize: 30,fontWeight: FontWeight.bold),),],)
                                ),
                                Expanded(
                                  child: Column(children: [Icon(Icons.severe_cold,color: Colors.white,),Text("Mean",style: TextStyle(color: Colors.white),),Text("Temp(c)",style: TextStyle(color: Colors.white),),Text("15",style: TextStyle(color: Colors.white,fontSize: 30,fontWeight: FontWeight.bold),),],)
                                ),
                              ],
                            ),
                          ),
                          Expanded(
                            child: Column(
                              children: [
                                Expanded(
                                  child:Column(children: [Icon(Icons.sunny,color: Colors.white,),Text("Day",style: TextStyle(color: Colors.white),),Text("(hours)",style: TextStyle(color: Colors.white),),Text("24",style: TextStyle(color: Colors.white,fontSize: 30,fontWeight: FontWeight.bold),),],)
                                ),
                                Expanded(
                                  child: Column(children: [Icon(Icons.speed_outlined,color: Colors.white,),Text("Distance from",style: TextStyle(color: Colors.white),),Text("Sun(mil km)",style: TextStyle(color: Colors.white),),Text("147.94",style: TextStyle(color: Colors.white,fontSize: 30,fontWeight: FontWeight.bold),),],)
                                ),
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    flex: 5,
                  ),
                  Container(
                    padding: const EdgeInsetsDirectional.only(bottom: 50),
                    child: SizedBox(
                      width: 146,
                      height: 48,
                      child: DecoratedBox(
                        decoration: BoxDecoration(
                          gradient: const LinearGradient(
                            begin: Alignment.bottomLeft,
                            end: Alignment.topRight,
                            colors: [
                              Color(0xFFE961FF),
                              Color(0xFF72A5F2),
                              Color(0xFF0AFBFB),
                            ],
                          ),
                          borderRadius: BorderRadius.circular(28),
                        ),
                        child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                            backgroundColor: Colors.transparent,
                            disabledBackgroundColor: Colors.transparent,
                            shadowColor: Colors.transparent,
                          ),
                          onPressed: () {
                            // Respond to button press
                          },
                          child: const Text(
                            "Visit",
                            style: TextStyle(
                                color: Colors.white70,
                                fontSize: 24,
                                fontWeight: FontWeight.w700),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Container(
              margin: const EdgeInsetsDirectional.only(start: 127, top: 208),
              decoration: BoxDecoration(
                borderRadius: BorderRadiusDirectional.circular(60),
              ),
              child: const Image(
                height: 120,
                image: AssetImage("assets/images/earth.png"),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
