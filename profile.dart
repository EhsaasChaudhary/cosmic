import 'package:flutter/material.dart';

class Profilescreen extends StatefulWidget {
  const Profilescreen({Key? key}) : super(key: key);

  @override
  State<Profilescreen> createState() => _ProfilescreenState();
}

class _ProfilescreenState extends State<Profilescreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/allscreen-bg.png"),
                    fit: BoxFit.cover),
              ),
            ),
          ),
          Column(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: const Color(0x90091522),
                    borderRadius: const BorderRadiusDirectional.only(
                      bottomEnd: Radius.circular(35),
                      bottomStart: Radius.circular(35),
                    ),
                    border: Border.all(
                      width: 1,
                      color: const Color(0xFF3A3A42),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Container(
                        margin:
                            const EdgeInsetsDirectional.only(start: 24, top: 15),
                        height: 48,
                        width: 48,
                        child: DecoratedBox(
                          decoration: BoxDecoration(
                            color: const Color(0x70091522),
                            borderRadius: BorderRadiusDirectional.circular(28),
                            border: Border.all(
                              width: 1,
                              color: const Color(0xFF3A3A42),
                            ),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              backgroundColor: Colors.transparent,
                              disabledBackgroundColor: Colors.transparent,
                              shadowColor: Colors.transparent,
                            ),
                            onPressed: () {
                              // Respond to button press
                            },
                            child: const Icon(Icons.arrow_back_sharp),
                          ),
                        ),
                      ),
                      Center(child: Text("Profile",style: TextStyle(fontSize: 20),),)
                    ],
                  ),
                ),
                flex: 2,
              ),
              Expanded(
                child: Container(),
                flex: 5,
              ),
              Expanded(
                child: Container(),
                flex: 5,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
